#include <stdio.h>

int main(){
  /*
     "r" read
     "a" append
     "w" write
  */
  FILE *fpointer = fopen("employee.txt", "w");
  fprintf(fpointer, "Jim,Janitor\nPam,CEO\nSati,CTTO\n");

  fclose(fpointer);
  return 0;
}
