#include <stdio.h>

int main () {
  int age;
  printf("Enter your age:\n");
  scanf("%d", &age);
  /* & is a pointer;
     when using scanf and want to store user info in a var */

  char grade;
  printf("\nEnter your grade: ");
  scanf("%c", &grade);

  int nameLen = 20;
  char name[nameLen];
  printf("\nEnter your name (max %d chars): ", nameLen);
  fgets(name, nameLen, stdin);

  printf("\n+++++RESULTS++++\n");
  printf("Name: %s\n", name);
  printf("Age: %d years old\n", age);
  printf("Grade: %c\n", grade);
  printf("+++++END_RESULTS++++\n");
    
  return 0;
}
