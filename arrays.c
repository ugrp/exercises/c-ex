#include <stdio.h>

int main (){
  /* the lenght of the array is fixed to 10 */
  int luckyNumbers[10] = {3, 5, 9, 50, 90, 500};
  luckyNumbers[9] = 33;
  printf("Selected number: %d\n", luckyNumbers[9]);
  return 0;
}
