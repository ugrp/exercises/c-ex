#include <stdio.h>

int main(){
  int index = 1;
  while(index < 5) {
    printf("while %d\n", index);
    index++;
  }

  int index2 = 1;
  do {
    printf("do %d\n", index2);
    index2++;
  }
  while(index2 < 5) ;
  return 0;
}
