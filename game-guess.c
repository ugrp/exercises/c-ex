#include <stdio.h>
#include <time.h>
#include <stdlib.h>
int main(){
  int guess;
  int guessCount = 0;
  int guessLimit = 100;
  int outOfGuesses = 0;

  srand(time(NULL));
  int secretNumber = rand();

  while ((guess != secretNumber) && outOfGuesses == 0) {
    if (guessCount < guessLimit){
      if (guess) {
	if (secretNumber < guess) {
	  printf("%d is too high\n", guess);
	} else if (secretNumber > guess) {
	  printf("%d is too low\n", guess);
	}
	printf("Guesses left: %d\n", guessLimit - guessCount);
      }
      printf("Enter a number max (%d): ", RAND_MAX);
      scanf("%d", &guess);
      guessCount++;
    } else {
      outOfGuesses = 1;
    }
  }

  if (outOfGuesses == 1) {
    printf("You loose! It was %d", secretNumber);
  } else {
    printf("You win! It was %d (guessed: %d)!", secretNumber, guess);
  }
  printf("\n");
  
  return 0;
}
