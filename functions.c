/* for printf */
#include <stdio.h>

/* prototyping, so our functions are available bellow the main() fn */
/* a function that returns a double, and accept a paremeter "num", a double */
double cube(double num);

/* a function that returns an integer */
int main(){
  /* %f because we expect to get a double back from cube() */
  double userChoice;
  printf("Chose a number: ");
  scanf("%lf", &userChoice);
  printf("%f", cube(userChoice));
  return 0;
}

double cube(double num) {
  double result = num * num * num;
  return result;
}
