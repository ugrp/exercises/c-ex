#include <stdio.h>

/* a pointer is just a memory address inside the ram of the computer;
   it is just like in int, an other type of data, memory address */

int main() {

  int age = 30;
  int * pAge = &age;

  double gpa = 3.4;
  double * pGpa = &gpa;

  /* "&" gives the "physical address of the variable" */
  printf("age %p\n", &age);
  printf("pAge %p\n", pAge);

  printf("gpa %p\n", &gpa);
  printf("pGpa %p\n", pGpa);
  return 0;
}
