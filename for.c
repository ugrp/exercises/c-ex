#include <stdio.h>

int main(){
  for(int i=0; i < 15; i++) {
    printf("hello %d\n", i);
  }

  int luckyNumbers[] = {99, 32, 11, 123};
  for(int i=0; i < 20; i++) {
    printf("lucky %d\n", luckyNumbers[i]);
  }

  return 0;
}
