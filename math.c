#include <stdio.h>
#include <math.h>

int main () {
  int age;
  printf("%s: \n", "Enter your age");

  /* & is a pointer;
     when using scanf and want to store user info in a var */
  scanf("%d", &age);

  printf("Your are %d years old", age);


  double gpa;
  printf("%s: \n", "Enter your GPA");

  /* & is a pointer;
     when using scanf and want to store user info in a var */
  scanf("%lf", &gpa);

  printf("Your GPA is %f", gpa);

  char grade;
  printf("%c: \n", "Enter your grade");

  /* & is a pointer;
     when using scanf and want to store user info in a var */
  scanf("%c", &grade);

  printf("Your grade is %c", grade);
  
  return 0;
}
