#include <stdio.h>
#include <stdlib.h>

int main(int argc,  char *argv[]) {
  int maxSize;

  if (argc == 2) {
    maxSize = strtol(argv[1], NULL, 10);
  } else {
    printf("1 argument expected (integer).\n");
    return 1;
  }

  printf("** Pyramid of %i **", maxSize);

  /* printf("Size of your pyramid: "); */
  /* scanf("%d", &maxSize); */

  for (int i = 0; i <= maxSize && i <= 30; i++) {
    for (int j = 0; j < i; j++) {
      printf("* ");
    }
    printf("\n");
  }
  return 0;
}
