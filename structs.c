/* for printf */
#include <stdio.h>
/* for stricpy */
#include <string.h>


struct Student{
  char name[50];
  char major[50];
  int age;
  double gpa;
};

int main() {
  struct Student student1;
  student1.age = 22;
  student1.gpa = 3.2;
  /* copy a string to a variable */
  strcpy(student1.name, "Jim");
  strcpy(student1.major, "IT");

  printf("%s", student1.major);
  
  return 0;
}
