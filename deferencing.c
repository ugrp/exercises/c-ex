#include <stdio.h>

int main(){
  int age = 30;
  int *pAge = &age;

  /* dereferencing, 
     so use %d (number/digit/integer) instead of %p (pointer) */
  printf("%d\n", *pAge);

  return 0;
}
