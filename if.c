#include <stdio.h>

int max(int num1, int num2, int num3){
  int result;
  if((num1 > num2) || !(num3 < 2)){
    result = num2;
  }
  return result;
}

int main (){
  printf("%d\n", max(13,44, 55));
  printf("%d\n", max(981072,12987213,98509857));
  return 0;
}
