Compile with `gcc file_name.c`.

Run the output with `./a.out`.

`M-x compile` (g)

Intros & docs:

- https://youtu.be/KJgsSFOSQv0
- https://www.programiz.com/c-programming
- https://www.gnu.org/software/gnu-c-manual
- https://www.tutorialspoint.com/cprogramming
