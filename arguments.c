#include <stdio.h>

/* 
   compile and try with:
   ./a.out -p de
*/

int main(int argc, char *argv[]){
  printf("argc %d\n", argc);

  /* there are supplied arguments, through the cli */
  if(argc >= 2 ) {
    for (int i = 0; i <= argc; i++) {
      printf("argv[%d] %s\n", i, argv[i]);
    }
  } else if( argc > 3 ) {
    printf("Too many arguments supplied.\n");
  } else {
    printf("One argument expected.\n");
  }

  return 0;
}
