#include <stdio.h>

int main () {
  const int maxSize = 20;
  char color[maxSize];
  char pluralNoun[maxSize];

  /* We need 2, because we cannot store spaces from scanf */
  char personFirst[maxSize];
  char personLast[maxSize];

  printf("Enter a color: ");
  scanf("%s",  &color);

  printf("Enter a plural nounn: ");
  scanf("%s", &pluralNoun);

  printf("Enter a person FIRST name: ");
  scanf("%s", &personFirst);

  printf("Enter a person LAST name: ");
  scanf("%s", &personLast);
  
  printf("Roses are %s\n", color);
  printf("%s are blue\n", pluralNoun);
  printf("I love %s %s\n", personFirst, personLast);

  return 0;
}
