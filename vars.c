#include <stdlib.h>
#include <stdio.h>

int main()
{
  char characterName[] = "John";
  int characterAge = 35;
  printf("There is a person named %d\n", characterAge);
  printf("There is a person named %s\n", characterName);
  return 0;
}
