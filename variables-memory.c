#include <stdio.h>

int main(){
  int age = 30;
  double gpa = 3.4;
  char grade = 'A';

  /* this prints a pointer, reference of the memory for the variable */
  printf("%p\n", &grade);
  printf("%p\n", &gpa);
  printf("%p\n", &age);
  
  return 0;
}
