#include <stdio.h>

int main(){
  char grade;

  printf("What grade di you get? ");
  scanf("%c", &grade);

  switch(grade) {
  case 'A' :
    printf("you are good");
    break;
  case 'B':
    printf("you are almost good");
    break;
  case 'C':
    printf("you are not so good today..!");
    break;
  case 'D':
    printf("Almost hitting rock bottom!!??..!");
    break;
  case 'F':
    printf("plouf plouf!!??..!");
    break;
  default :
    printf("Invalid grade: %C (A-F)", grade);
  }
  return 0;
}  
